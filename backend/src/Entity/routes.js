const express = require ('express');
const crypto = require('crypto');
const connection  = require('../Repository/database/connection');

const OngController = require('../Controller/OngController');
const IncidentController = require('../Controller/IncidentController');
const ProfileController = require('../Controller/ProfileController');
const SessionController = require('../Controller/SessionController')

const routes = express.Router();

routes.post('/sessions',SessionController.create);

routes.get('/profile', ProfileController.index);

routes.post('/incidents', IncidentController.create);
routes.get('/incidents', IncidentController.index);
routes.delete('/incidents/:id',IncidentController.delete);

routes.get('/ongs', OngController.index);
routes.post('/ongs',OngController.create);


module.exports = routes;
