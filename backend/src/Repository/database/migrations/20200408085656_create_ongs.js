
exports.up = function(knex) {
    return knex.schema.createTable('ongs', function(table){
        table.string('id').primary();
        table.string('name').notNull();
        table.string('email').notNull();
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('ongs');
};
