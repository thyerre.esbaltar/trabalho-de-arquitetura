const express = require('express');
const cors = require('cors')
const routes = require('./Entity/routes');


const app = express();

app.use(cors()); //dar uma olhada depois

app.use(express.json());

app.use(routes);

app.listen(3333);
